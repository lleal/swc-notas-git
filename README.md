# Pequenas notas sobre o Git (incluindo servidores remotos)

Criação do projeto:
	
	$ git init

Adição de arquivo para ser monitorado:

	$ git add arquivo 

Obter informações sobre o projeto:

	$ git status

Criação de uma nova versão:
	
	$ git add -m 'descrição do commit'

Obter histórico de versões:
	
	$ git log

Para alterar a versão atual:

	$ git checkout versão
	
Para enviar alterações para um repositório remoto:

    $ git push -u origin master

Para contribuir com um projeto você precisa forkar o projeto, enviar modificações para sua cópia e criar um pull request.
